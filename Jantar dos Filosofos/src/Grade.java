
import javax.swing.JPanel;
import java.awt.*;

public class Grade extends JPanel implements Runnable{


	final static int N_FILOSOFOS = 40;


    final int PENSANDO = 0;
    final int FAMINTO  = 1;
    final int COMENDO  = 2;


    String mensagem = "";


    Thread animador;




    public static Semaforo mutex = new Semaforo(1);


    public static Semaforo semaforos[] = new Semaforo[N_FILOSOFOS];

    // Define um vetor para o estado de cada um dos fil�sofos presentes
    // na aplica��o
    public static int estado[] = new int[N_FILOSOFOS];


    static Filosofo filosofo[] = new Filosofo[N_FILOSOFOS];


    public Grade ()
    {

        setFocusable(true);


        setSize(400, 400);


        setBackground(Color.white);

        init();
    }


    public void init ()
    {

        for (int i = 0; i < estado.length; i++)
        {
            estado[i] = 0;
        }


        if(animador == null)
        {

            animador = new Thread(this);

            animador.start();
        }


        Thread.currentThread().setPriority(1);

        for(int i=0;i<N_FILOSOFOS;i++){
        	filosofo[i] = new Filosofo(Integer.toString(i+1),i);
        	semaforos[i] = new Semaforo(0);
        	filosofo[i].start();
        }

        for(int i=0;i<N_FILOSOFOS;i++){
        	try{
        		filosofo[i].join();
        	}catch(InterruptedException e){
        		System.out.println("Error> ");
        	}
        }



    }


    @Override
    public void paint(Graphics g)
    {
        super.paint(g);


        g.setColor(Color.blue);

        g.drawOval(50, 50, 300, 300);


        for(int i = 0; i < 5; i++)
        {
            if(estado[i] == 0)
            {
                g.setColor(Color.gray);
                mensagem = "PENSANDO";
            }
            if(estado[i] == 1)
            {
                g.setColor(Color.yellow);
                mensagem = "FAMINTO";
            }
            if(estado[i] == 2)
            {
                g.setColor(Color.green);
                mensagem = "COMENDO";
            }


            g.fillOval((int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) - 15, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) - 15, 30, 30);
            g.setColor(Color.black);
            g.drawLine((int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) - 5, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) + 5, (int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) + 5, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) + 5);
            g.drawLine((int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) - 2, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) - 3, (int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) + 2, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)));
            g.drawLine((int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) - 2, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)), (int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) + 2, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)));
            g.drawLine((int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) - 8, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) - 8, (int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) - 3, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) - 8);
            g.drawLine((int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) + 3, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) - 8, (int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) + 8, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) - 8);
            g.drawString(filosofo[i].getName(), (int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) - 15, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) + 25);
            g.drawString(mensagem, (int)(200D - 100D * Math.cos(1.2566370614359172D * (double)i)) - 15, (int)(200D - 100D * Math.sin(1.2566370614359172D * (double)i)) + 40);
        }


        Toolkit.getDefaultToolkit().sync();
        g.dispose();

    }

    public void run ()
    {
        do
        {
            repaint();

            try
            {
                Thread.sleep(1000L);
            }
            catch (InterruptedException ex)
            {
                System.out.println("ERROR>" + ex.getMessage());
            }
        }
        while (true);
    }

}
